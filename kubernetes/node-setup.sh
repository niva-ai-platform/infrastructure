#! /bin/bash

echo $1 $2 $3
remoteKey="~/.ssh/ktest-master-key"
remoteLogin="ubuntu@${1}"
scp -i $remoteKey kb8-setup.sh $remoteLogin:.
ssh -i $remoteKey $remoteLogin "./kb8-setup.sh"
#ssh -i $remoteKey $remoteLogin "sudo kubeadm join ${1}:6443 --token ${2} --discovery-token-ca-cert-hash ${3} --cri-socket /run/cri-dockerd.sock"
