#! /bin/bash

sudo apt update
sudo apt -y full-upgrade
[ -f /var/run/reboot-required ] && sudo reboot now

sudo apt install -y git curl wget gnupg2 software-properties-common apt-transport-https ca-certificates

# install nfs client
sudo apt -y install nfs-common

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt update
sudo apt -y install vim git curl wget kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl

# white space around swap
sudo sed -i.bak '/\sswap\s/s/^/#/' /etc/fstab
sudo swapoff -a

# Enable kernel modules
sudo modprobe overlay
sudo modprobe br_netfilter

# Add some settings to sysctl
sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

# Reload sysctl
sudo sysctl --system

# Add repo and Install
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update
sudo apt install -y containerd.io docker-ce docker-ce-cli

# Create required directories
sudo mkdir -p /etc/systemd/system/docker.service.d

# Create daemon json config file
sudo tee /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

# Start and enable Services
sudo systemctl daemon-reload
sudo systemctl restart docker
sudo systemctl enable docker

# install a docker shim as docker has been depreciated
CRIVER=$(curl -s https://api.github.com/repos/Mirantis/cri-dockerd/releases/latest|grep tag_name | cut -d '"' -f 4|sed 's/v//g')
wget https://github.com/Mirantis/cri-dockerd/releases/download/v${CRIVER}/cri-dockerd-${CRIVER}.amd64.tgz
tar xvf cri-dockerd-${CRIVER}.amd64.tgz
sudo mv cri-dockerd/cri-dockerd /usr/local/bin/

wget https://raw.githubusercontent.com/Mirantis/cri-dockerd/master/packaging/systemd/cri-docker.service
wget https://raw.githubusercontent.com/Mirantis/cri-dockerd/master/packaging/systemd/cri-docker.socket
sudo mv cri-docker.socket cri-docker.service /etc/systemd/system/
sudo sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service

sudo systemctl daemon-reload
sudo systemctl enable cri-docker.service
sudo systemctl enable --now cri-docker.socket

if [ $1 = 'master' ]; then
  sudo kubeadm init --cri-socket /run/cri-dockerd.sock
  #  --pod-network-cidr=192.168.0.0/16

  # post install setup
  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

  # setup dns
  kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

  # add docker registry secret
  defaultRegServer="gitlab-ee.waltoninstitute.ie:5050"
  defaultRegServerUsername="niva-ai-container-registry-ro"
  defaultRegServerEmail="niva-ai@waltoninstitute.ie"

  read -p "Enter your registry server [$defaultRegServer]: " regServer
  regServer=${regServer:-$defaultRegServer}

  read -p "Enter your registry server username [$defaultRegServerUsername]: " regServerUsername
  regServerUsername=${regServerUsername:-$defaultRegServerUsername}

  read -p "Enter your registry server email [$defaultRegServerEmail]: " regServerEmail
  regServerEmail=${regServerEmail:-$defaultRegServerEmail}

  read -p "Enter your registry server Password: " regServerPassword

  kubectl create secret docker-registry regcred --docker-server=$regServer --docker-username=$regServerUsername --docker-password=$regServerPassword --docker-email=$regServerEmail

  # install knative operator
  #kubectl apply -f https://github.com/knative/operator/releases/download/knative-v1.5.1/operator.yaml
fi

# if [ $1 = 'ingress' !! $2 = 'ingress' ]; then
#   git clone https://github.com/nginxinc/kubernetes-ingress.git --branch v2.3.0
#   cd kubernetes-ingress/deployments

#   kubectl apply -f common/ns-and-sa.yaml
#   kubectl apply -f rbac/rbac.yaml
#   kubectl apply -f rbac/ap-rbac.yaml
#   kubectl apply -f rbac/apdos-rbac.yaml

#   kubectl apply -f common/default-server-secret.yaml
#   kubectl apply -f common/nginx-config.yaml
#   kubectl apply -f common/ingress-class.yaml

#   kubectl apply -f common/crds/k8s.nginx.org_virtualservers.yaml
#   kubectl apply -f common/crds/k8s.nginx.org_virtualserverroutes.yaml
#   kubectl apply -f common/crds/k8s.nginx.org_transportservers.yaml
#   kubectl apply -f common/crds/k8s.nginx.org_policies.yaml

#   kubectl apply -f common/crds/k8s.nginx.org_globalconfigurations.yaml

#   kubectl apply -f deployment/nginx-ingress.yaml

#   kubectl create -f service/nodeport.yaml
# fi
