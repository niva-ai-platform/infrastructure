locals {
  # resource-base = "${var.base-name}-${var.base-env}"
    resource-base = "${var.base-name}-nfs-${terraform.workspace}"
}

resource "openstack_compute_instance_v2" "nfs-server" {
  name            = "${local.resource-base}"
  image_name      = "${var.linux-image}"
  flavor_name     = "W8.2C.4G.40"
  key_pair        = "ktest-master-key"
  security_groups = ["default", "wfh", "nfs"]
  # user_data = "#!/bin/bash\n sudo gitlab-runner verify"

  network {
    name = "Walton-ext-dual-net"
  }
}

resource null_resource "upload-file" {
  triggers = {
    instance_id = "${openstack_compute_instance_v2.nfs-server.id}"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = "${file("~/.ssh/ktest-master-key")}"
    host        = "${openstack_compute_instance_v2.nfs-server.access_ip_v4}"
    agent       = false
  }

  # provisioner "file" {
  #   source = "~/.ssh/msu.tssg.org.key"
  #   destination = "/home/ubuntu/.ssh/msu.key"
  # }

  # provisioner "file" {
  #   source = "./docker-setup.sh"
  #   destination = "/home/ubuntu/docker-setup.sh"
  # }

  provisioner "file" {
    source = "./nfs-setup.sh"
    destination = "/home/ubuntu/nfs-setup.sh"
  }

  provisioner "remote-exec" {
    inline = [
#      "chmod +x docker-setup.sh",
      "chmod +x nfs-setup.sh",
      "sudo ./nfs-setup.sh"
    ]
  }
}

resource "openstack_blockstorage_volume_v2" "nfs-volume" {
  name = "${local.resource-base}-volume"
  size = var.data-volume-size
}

resource "openstack_compute_volume_attach_v2" "nfs" {
  instance_id = openstack_compute_instance_v2.nfs-server.id
  volume_id   = openstack_blockstorage_volume_v2.nfs-volume.id
}

# Output VM IP Address
output "serverip" {
  value = openstack_compute_instance_v2.nfs-server.access_ip_v4
}

output "volume" {
  value = openstack_compute_volume_attach_v2.nfs.id
}
