#! /bin/bash

sudo apt-get install nfs-kernel-server -y
sudo systemctl enable --now nfs-server

# find mounted volumes and save to variables
volume1=$(lsblk | grep -o 'vd.\{1\}' | tail -2 | head -n 1)
volume2=$(lsblk | grep -o 'vd.\{1\}' | tail -1)

# # create new partitions
printf '%s\n' n '' '' '' '' w | sudo fdisk /dev/$volume1
printf '%s\n' n '' '' '' '' w | sudo fdisk /dev/$volume2

# format new partitions
partition1="${volume1}1"
partition2="${volume21}1"
sudo mkfs.ext4 /dev/$partition1
sudo mkfs.ext4 /dev/$partition2
printf '\nPartitions formatted\n\n'

# ceate data volume mount points
sudo mkdir /mnt/data/dev
sudo mkdir /mnt/data/test

# create nfs share mount points
sudo mkdir -p /srv/data/dev
sudo mkdir -p /srv/data/test

# add mount point to fstab file
newLine1=$(echo "/dev/$partition1 /mnt/data/dev ext4 defaults 0 0")
newLine2=$(echo "/dev/$partition2 /mnt/data/test ext4 defaults 0 0")
newLine3=$(echo "/mnt/data/dev /srv/data/dev none bind 0 0")
newLine4=$(echo "/mnt/data/test /srv/data/test none bind 0 0")
echo $newLine1 | sudo tee -a /etc/fstab
echo $newLine2 | sudo tee -a /etc/fstab
echo $newLine3 | sudo tee -a /etc/fstab
echo $newLine4 | sudo tee -a /etc/fstab

# test new fstab setting, test drive is mounted:
sudo mount -a

# set up export directories
sudo mkdir -p /srv/data/dev/image-storage
sudo mkdir -p /srv/data/dev/mysql
sudo mkdir -p /srv/data/test/image-storage
sudo mkdir -p /srv/data/test/mysql
sudo chown -R nobody:nogroup /srv/data

# export the shared directory
newLine5=$(echo "/srv/data 87.44.17.0/24(rw,sync,no_subtree_check,crossmnt,fsid=0)")
newLine6=$(echo "/srv/data/dev/image-storage 87.44.17.0/24(rw,sync,no_subtree_check,crossmnt)")
newLine7=$(echo "/srv/data/dev/mysql 87.44.17.0/24(rw,sync,no_subtree_check,no_root_squash,crossmnt)")
newLine8=$(echo "/srv/data/test/image-storage 87.44.17.0/24(rw,sync,no_subtree_check,crossmnt)")
newLine9=$(echo "/srv/data/test/mysql 87.44.17.0/24(rw,sync,no_subtree_check,no_root_squash,crossmnt)")

echo $newLine5 | sudo tee -a /etc/exports
echo $newLine6 | sudo tee -a /etc/exports
echo $newLine7 | sudo tee -a /etc/exports
echo $newLine8 | sudo tee -a /etc/exports
echo $newLine9 | sudo tee -a /etc/exports

sudo exportfs -ar
sudo exportfs -v
