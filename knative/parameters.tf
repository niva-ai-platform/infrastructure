variable "base-name" {
  description = "the base name for the resources"
  default = "nivaAi"
}

variable "base-env" {
  description = "deployment environment"
  default = "knative-dev"
}
