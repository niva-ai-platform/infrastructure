#! /bin/bash

sudo apt update
sudo apt -y full-upgrade
[ -f /var/run/reboot-required ] && sudo reboot now

# Add repo and Install packages
sudo apt update
sudo apt install -y curl gnupg2 software-properties-common apt-transport-https ca-certificates
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update
sudo apt install -y containerd.io docker-ce docker-ce-cli docker-compose-plugin

# Create required directories
sudo mkdir -p /etc/systemd/system/docker.service.d

https://github.com/knative/client/tree/knative-v1.5.0

# Create daemon json config file
sudo tee /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

# Start and enable Services
sudo systemctl daemon-reload
sudo systemctl restart docker
sudo systemctl enable docker

curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.14.0/kind-linux-amd64
chmod +x ./kind
mv ./kind /usr/local/bin/kind

curl -Lo ./kn https://github.com/knative/client/releases/download/knative-v1.5.0/kn-linux-amd64
chmod +x ./kn
mv ./kn /usr/local/bin

curl -Lo ./kn-quickstart https://github.com/knative-sandbox/kn-plugin-quickstart/releases/download/knative-v1.5.1/kn-quickstart-linux-amd64
chmod +x ./kn-quickstart
mv ./kn-quickstart /usr/local/bin

os="linux/amd64" && curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/$os/kubectl"
chmod +x ./kubectl
mv ./kubectl /usr/local/bin
