# Setup Reverse Proxy

before running terrform, you need to be connectedto the walton network, either directly in the walton offices, or via vpn.

you then need to modify your hosts file to set cloud.waltoninstitute.ie to 10.37.93.100
1. Check parameters.tf
1. Check providers.tf
1. This tf script expects a key called ktest-master to exist in `~/.ssh`
1. Run `terraform init`
1. Run `terraform apply`
1. Note the ip address of the server
1. update deploy.sh with new ip address
1. ssh to created server
1. Run `docker-setup.sh
1. if reboot occures repeat go back to step 8
1. `cd traefik`
1. run `docker compose up -d`
