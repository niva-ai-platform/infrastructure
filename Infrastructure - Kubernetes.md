# Instructions for the installation of Kubernetes and Docker on the set of Nodes provisioned by Terraform 
- Make updates to the Master Node 
- Install required applications on the Slave Nodes

## 1 - Configuration on the Master Node
The VMs have now been created with the **"5GSol.tssg.org.key"** public key. The terraform script has also uploaded the private key to the Master Node so that it can be used to log into each of the Slave Nodes.  
> **NOTE:** If using PUTTY to log into the Master VM you now need to convert the private key into a "ppk" for use with putty.  

The configuration of the Slave Nodes are completed by using the Master Node as a Bastian/Proxy Host to gain access to each of the Slave Nodes. 

To get access from the Master to the Slave nodes one of the following methods is required: 
- Log into the Master Node using SSH and add the **"5GSol.tssg.org.key"** private key to the SSH Agent so that it can be used by SSH to log into the Slave Nodes:  
````
eval "$(ssh-agent)"
ssh-add ~/.ssh/5GSol.tssg.org.key 
ssh ubuntu@<SlaveNode-IPAddress>
````  
**OR**  
- Rename the key to one of the defaults that the SSH agent looks for to be present in the ~/.ssh/ folder to use: "id_rsa":
````  
cp ~/.ssh/5GSol.tssg.org.key ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
````  
**The latter has been implemented automatically in the Terraform "deploy" script.**  
The Master Node should now be able to log into each of the Slave Nodes.  


### Verify the OS Firewall & Security Groups are configured correctly
There are various ports utilised  by Kubernetes that are well documented and as such should be configured in the Firewall/Security Group settings for the Kubernetes VMs. There is little to no info on how to configure the firewall for the Weave CNI Pod Networking until I found this:  
https://www.weave.works/docs/net/latest/faq/  
These ports must also be configured in the Firewall/Security Group settings for the Kubernetes VMs.  


### 1.1 - Disable Swap
On the master node make sure that swap is disabled. Execute the following to disable Swap: 
````
sudo swapoff -v /swap.img     - Disable Swap
sudo vi /etc/fstab            - Remove the line describing the Swap location
sudo rm /swap.img             - Remove the swapfile itself
sudo shutdown -r now
````

### 1.2 Configure Networking
Make sure that the br_netfilter module is loaded. This can be done by running lsmod | grep br_netfilter. To load it explicitly call sudo modprobe br_netfilter.  

It is only required to be enabled on the Master node so execute the following command:  
````
sudo sysctl net.bridge.bridge-nf-call-iptables=1
````

As a requirement for your Linux Node's iptables to correctly see bridged traffic, you should ensure net.bridge.bridge-nf-call-iptables is set to 1 in your sysctl config, e.g.  
````
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system
````
### 1.3 Copy Setup File to the Slave Nodes
On the master node the setup files required for both **Docker** and **Kubernetes** are located in the **~/5GSolutions/** folder.  
![MasterNode-5GSolDirectory01][MasterNode-5GSolDirectory01]  

The contents of the **"~/5GSolutions/"** directory on the Master Node needs to be copied to each of the Slave Nodes. SCP the folder to each of the Slave Nodes using the following:  
````
scp -r /home/ubuntu/5GSolutions ubuntu@<SlaveNode-IPAddress>:/home/ubuntu/
````

## 2 - Setup Master Node with Docker & Kubernetes  
On the master node execute the following: 
````
sudo su
cd /home/ubuntu/5GSolutions
./docker-setup.sh
./kb8-setup.sh
kubeadm init
exit
````
  
Note the output from the **"kubeadm init"** command, there is information that needs to be retained as each configured slave node requires this information in order to join the kubernetes cluster. 
````
[bootstrap-token] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[bootstrap-token] Creating the "cluster-info" ConfigMap in the "kube-public" namespace
[kubelet-finalize] Updating "/etc/kubernetes/kubelet.conf" to point to a rotatable kubelet client certificate and key
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 172.16.4.77:6443 --token okjaes.g7rudb3o9qtot4cx \
    --discovery-token-ca-cert-hash sha256:7d9ae8b9f737ed73b72a48080a75b5ff6abb01473268d9c902bd672cf6e05219 
root@5gsol-master:/home/ubuntu/5GSolutions#
````

As the regular (ubuntu) user execute the following commands on the master node: 
````
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config 
````
Again just before we install the CNI (Container Network Interface: WeaveNet) we need to execute the following command again to make sure its configured prior to installation of the CNI:
````
sudo sysctl net.bridge.bridge-nf-call-iptables=1
````
Now we can install the CNI for the cluster: 
````
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
````
Execute this command to verify that the master is ready and that no nodes have connected so far: 
````
kubectl get nodes
````

Once a Pod network has been installed, you can confirm that it is working by checking that the CoreDNS Pod is Running in the output of kubectl get pods --all-namespaces. And once the CoreDNS Pod is up and running, you can continue by joining your nodes.

If your network is not working or CoreDNS is not in the Running state, check out the troubleshooting guide for kubeadm:  

https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/troubleshooting-kubeadm/   


The master node is now ready for the slave node to connect to it. 


## 3 - Setup the Slave Nodes with Docker & Kubernetes
### 3.1 Configure Networking
Make sure that the br_netfilter module is loaded. This can be done by running lsmod | grep br_netfilter. To load it explicitly call sudo modprobe br_netfilter.  

Execute the following command:  
````
sudo sysctl net.bridge.bridge-nf-call-iptables=1
````
As a requirement for your Linux Node's iptables to correctly see bridged traffic, you should ensure net.bridge.bridge-nf-call-iptables is set to 1 in your sysctl config, e.g.  
````
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system
````
### 3.2 Install Docker & Kubernetes
The script files for building the infrastructure is on each of the Slave Nodes and are ready to be executed, note that you must execute these commands as root: 
````
ssh ubuntu@<SlaveNode-IPAddress>
sudo su -
cd /home/ubuntu/5GSolutions
./docker-setup.sh
./kb8-setup.sh
kubeadm join 172.16.4.77:6443 --token okjaes.g7rudb3o9qtot4cx --discovery-token-ca-cert-hash sha256:7d9ae8b9f737ed73b72a48080a75b5ff6abb01473268d9c902bd672cf6e05219
exit
````
When completed execute the following on the master node to see if the slave node has joined correctly: 
````
kubectl cluster-info
kubectl get nodes
kubectl get all
kubectl get pods --all-namespaces -o wide
````

The results of a clean newly created kubernetes cluster should look like this: 
![NewlyCreatedK8nCluster_BasicCommandsExecuted][NewlyCreatedK8nCluster_BasicCommandsExecuted]  


If you lose the token or it times out before you join the nodes have a look here on how to recreate a new token and get the key out of that token to use in the new join command:  
https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/#join-nodes  
````
kubeadm token create --help
kubeadm token create list --help
kubeadm token create --print-join-command
````


### 3.3 - Persistant Storage for the DBs on one of the Slave nodes
In this case we are using SlaveNode02 for the deployment of the MongoDB containers. In the host node we will configure the container to use a mapped volume to write all of its data to so that the data is actually stored off VM in a volume attached to the VM itself via OpenStack.  

Look in the "dev/disk/by-id" folder to see what the attached volume id is, in this case it was: 
````
virtio-3ab11286-2868-49e7-b -> ../../vdb
````

Query the volume to see if it is what is expected:
````
sudo fdisk /dev/vdb -l
````

Commands to format and mount the volume:
````
sudo mkfs.ext4 /dev/disk/by-id/virtio-3ab11286-2868-49e7-b
sudo mkdir -p /mnt/5gsoldbbackups
sudo mount /dev/disk/by-id/virtio-3ab11286-2868-49e7-b /mnt/5gsoldbbackups
````

Edit the **"/etc/fstab"** file so that the volume is mounted if the VM is restarted: 
````
UUID="d4f0fd66-0b61-41d5-aa21-2cae25994e29" /mnt/5gsoldbbackups ext4 defaults 0 0
````

## 4 - Reconfigure the TLS certificate used by the Kubernetes API server to allow for remote administration  
https://blog.scottlowe.org/2019/07/30/adding-a-name-to-kubernetes-api-server-certificate/  

The Kubernetes API server uses digital certificates to both encrypt traffic to/from the API server as well as to authenticate connections to the API server. As such, if you try to connect to the API server using a command-line client like **"kubectl"** and you use a hostname or IP address that isn’t included in the certificate’s list of Subject Alternative Names (SANs), you’ll get an error indicating that the certificate isn’t valid for the specified IP address or hostname. To fix that, you need to update the certificate so that the list of SANs includes any and all IP addresses or hostnames that you will use to access the API server.  
Pull the kubeadm config file from the cluster into a file somewhere local:  
````
ssh ubuntu@<MasterNodeIPAddress>
cd ~/5GSolutions
kubectl -n kube-system get configmap kubeadm-config -o jsonpath='{.data.ClusterConfiguration}' > kubeadm.yaml
````

Should look like this:  
````
ubuntu@5gsol-master:~/5GSolutions$ cat kubeadm.yaml 
apiServer:
  extraArgs:
    authorization-mode: Node,RBAC
  timeoutForControlPlane: 4m0s
apiVersion: kubeadm.k8s.io/v1beta2
certificatesDir: /etc/kubernetes/pki
clusterName: kubernetes
controllerManager: {}
dns:
  type: CoreDNS
etcd:
  local:
    dataDir: /var/lib/etcd
imageRepository: k8s.gcr.io
kind: ClusterConfiguration
kubernetesVersion: v1.18.3
networking:
  dnsDomain: cluster.local
  serviceSubnet: 10.96.0.0/12
scheduler: {}
ubuntu@5gsol-master:~/5GSolutions$ 
````

Add the following:
````
apiServer:
  certSANs:
  - "172.16.4.88"
  - "87.44.18.146"
  - "k8n.kpivs-5gsolutions.eu"
````

Result is the following:
````
ubuntu@5gsol-master:~/5GSolutions$ cat kubeadm.yaml 
apiServer:
  certSANs:
  - "172.16.4.88"
  - "87.44.18.146"
  - "k8n.kpivs-5gsolutions.eu"
  extraArgs:
    authorization-mode: Node,RBAC
  timeoutForControlPlane: 4m0s
apiVersion: kubeadm.k8s.io/v1beta2
certificatesDir: /etc/kubernetes/pki
clusterName: kubernetes
controllerManager: {}
dns:
  type: CoreDNS
etcd:
  local:
    dataDir: /var/lib/etcd
imageRepository: k8s.gcr.io
kind: ClusterConfiguration
kubernetesVersion: v1.18.3
networking:
  dnsDomain: cluster.local
  serviceSubnet: 10.96.0.0/12
scheduler: {}
ubuntu@5gsol-master:~/5GSolutions$
````

Next move the existing API server cert & key, they will not be overwritten if they already exist: 
````
sudo mv /etc/kubernetes/pki/apiserver.{crt,key} ~/5GSolutions
````

Use kubeadm to generate the new cert and key:  
````
sudo kubeadm init phase certs apiserver --config kubeadm.yaml
````

Results should be this: 
````
ubuntu@5gsol-master:~/5GSolutions$ sudo kubeadm init phase certs apiserver --config kubeadm.yaml
W0617 20:22:11.282662  181889 configset.go:202] WARNING: kubeadm cannot validate component configs for API groups [kubelet.config.k8s.io kubeproxy.config.k8s.io]
[certs] Generating "apiserver" certificate and key
[certs] apiserver serving cert is signed for DNS names [5gsol-master kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local k8n.kpivs-5gsolutions.eu] and IPs [10.96.0.1 172.16.4.88 172.16.4.88 87.44.18.146]
ubuntu@5gsol-master:~/5GSolutions$ 
````

The final step is restarting the API server to pick up the new certificate. The easiest way to do this is to kill the API server container using docker. (The container ID will be the very first field in the output):  
````
docker ps | grep kube-apiserver | grep -v pause
docker kill f1638f5d2b54
````

The Kubelet will automatically restart the container, which will pick up the new certificate. As soon as the API server restarts, you will immediately be able to connect to it using one of the newly-added IP addresses or hostnames.  
To verify execute:  
````
openssl x509 -in /etc/kubernetes/pki/apiserver.crt -text
````
Look for the “X509v3 Subject Alternative Name” line, after which will be a list of all the DNS names and IP addresses that are included on the certificate as SANs. After following this procedure, you should see the newly-added names and IP addresses you specified in the modified kubeadm configuration file.  
Final step is the upload the updated config file so that any further updates to the cluster config information will carry forward the changes made here.  
Execute the following: 
````
kubeadm config upload from-file --config kubeadm.yaml
````

**NOTE:** After upgrading Kubernetes to v1.19.2 it appears the command doesn't work anymore, the command has been depreciated for a while and must have been removed prior to this release. Used this instead: 
````
sudo kubeadm init phase upload-config kubeadm --config kubeadm.yaml
````
````
ubuntu@5gsol-master:~/5GSolutions$ sudo kubeadm init phase upload-config kubeadm --config kubeadm.yaml
W1012 20:54:12.365056  115958 configset.go:348] WARNING: kubeadm cannot validate component configs for API groups [kubelet.config.k8s.io kubeproxy.config.k8s.io]
[upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
ubuntu@5gsol-master:~/5GSolutions$ 
````
https://github.com/kubernetes/kubeadm/issues/988  

**End Note**


To verify the upload was successful: 
````
kubectl -n kube-system get configmap kubeadm-config -o yaml
````

The result should be similar to this:
````
apiVersion: v1
data:
  ClusterConfiguration: |
    apiServer:
      certSANs:
      - 172.16.4.88
      - 87.44.18.146
      - k8n.kpivs-5gsolutions.eu
      extraArgs:
        authorization-mode: Node,RBAC
      timeoutForControlPlane: 4m0s
    apiVersion: kubeadm.k8s.io/v1beta2
    certificatesDir: /etc/kubernetes/pki
    clusterName: kubernetes
    controllerManager: {}
    dns:
      type: CoreDNS
    etcd:
      local:
        dataDir: /var/lib/etcd
    imageRepository: k8s.gcr.io
    kind: ClusterConfiguration
    kubernetesVersion: v1.18.3
    networking:
      dnsDomain: cluster.local
      serviceSubnet: 10.96.0.0/12
    scheduler: {}
  ClusterStatus: |
    apiEndpoints:
      5gsol-master:
        advertiseAddress: 172.16.4.88
        bindPort: 6443
    apiVersion: kubeadm.k8s.io/v1beta2
    kind: ClusterStatus
kind: ConfigMap
metadata:
  creationTimestamp: "2020-06-17T13:01:02Z"
  managedFields:
  - apiVersion: v1
    fieldsType: FieldsV1
    fieldsV1:
      f:data:
        .: {}
        f:ClusterConfiguration: {}
        f:ClusterStatus: {}
    manager: kubeadm
    operation: Update
    time: "2020-06-17T20:33:05Z"
  name: kubeadm-config
  namespace: kube-system
  resourceVersion: "68306"
  selfLink: /api/v1/namespaces/kube-system/configmaps/kubeadm-config
  uid: 080fe905-c6c3-4562-8b24-db179304fbd4
````


## 5 - Create the required accounts, roles and secrets to support the deployment
Create the Service Account, the Cluster Role Binding and the Cluster Role. 

Yaml file for the Service Account "gitlab-admin_SA.yaml":
````
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin-binding
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: deployment-reader
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
````

Apply the Service Account: 
````
kubectl apply -f ~/5GSolutions/gitlab-admin_SA.yaml
````
````
serviceaccount/gitlab-admin created
clusterrolebinding.rbac.authorization.k8s.io/gitlab-admin-binding created
````

Yaml file for the Cluster Role "gitlab-admin_CR.yaml":
````
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: deployment-reader
rules:
- apiGroups: ["extensions", "apps"]
  resources: ["deployments"]
  verbs: ["get", "watch", "list"]
````

Apply the Cluster Role: 
````
kubectl apply -f ~/5GSolutions/gitlab-admin_CR.yaml
````
````
clusterrole.rbac.authorization.k8s.io/deployment-reader created
````

Create the secret required to access the container registry from kubernetes:  
https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  

From the Master node:
login to the docker registry: 
````
sudo docker login gitlab.kpivs-5gsolutions.eu:5050 -u k8n-cicd-without-auto-devops-token -p BHnZMzZRF-Y6yxKC4tY8
sudo cat /root/.docker/config.json
````
Produces this Output:
````
{
	"auths": {
		"gitlab.kpivs-5gsolutions.eu:5050": {
			"auth": "azhuLWNpY2Qtd2l0aG91dC1hdXRvLWRldm9wcy10b2tlbjpCSG5aTXpaUkYtWTZ5eEtDNHRZOA=="
		}
	},
	"HttpHeaders": {
		"User-Agent": "Docker-Client/19.03.11 (linux)"
	}
}
````

````
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=<path/to/.docker/config.json> \
    --type=kubernetes.io/dockerconfigjson
````

OR
````
kubectl create secret docker-registry regcred --docker-server=gitlab.kpivs-5gsolutions.eu:5050 --docker-username=k8n-cicd-without-auto-devops-token --docker-password=BHnZMzZRF-Y6yxKC4tY8 --docker-email=mtolan@tssg.org
````
````
secret/regcred created
````

Regardless of method of creation, use this to examine the generated secret: 
````
kubectl get secret regcred --output=yaml
````
The value of the .dockerconfigjson field is a base64 representation of your Docker credentials.
To understand what is in the .dockerconfigjson field, convert the secret data to a readable format:
````
kubectl get secret regcred --output="jsonpath={.data.\.dockerconfigjson}" | base64 --decode
````
To understand what is in the auth field, convert the base64-encoded data to a readable format:
````
echo "c3R...zE2" | base64 --decode
````

Notice that the Secret data contains the authorization token similar to your local ~/.docker/config.json file. You have successfully set your Docker credentials as a Secret called regcred in the cluster.  

The secret used to connect the kubernetes cluster to the container register is now configured and available for use by the deployment scripts within the CI/CD environment. This is the static configuration that should only be needed the once, all CD deployments should be able to use the secret multiple times.  


### 5.1: GitLab token times out - Need to rebuild K8s Secret
Kuberneres uses Access Tokens to get access to the Container Registry hosted by GitLab. To prevent the sharing if user credentials an Access Token is used instead. These can be configured to time out. When they do the associated K8s secret using that Access Token needs to be rebuilt.

````
kubectl get secrets
kubectl get secret regcred
kubectl delete secret regcred
````

In GitLab, under your user account (right click and Edit Profile on your profile in the top right hand side) select Access Tokens. Set permissions to API & SUDO, note the generated password for the token as it cant be accessed again.  
Log into docker from Master to verify that the Access Token is good, then create the new secret in K8s using these new credentials. 
````
kubectl create secret docker-registry regcred --docker-server=gitlab.kpivs-5gsolutions.eu:5050 --docker-username=k8s-cicd-devops-token --docker-password=f6oiMy2YwxBB5ka_hK8a --docker-email=mtolan@wit.ie
````


## 6 - Connecting to External IP Address 

====== Connect to external IPs ===================  

https://kubernetes.io/docs/concepts/services-networking/connect-applications-service/  

The load balancer seems to be best used for cloud vendor environments where the load balancer is usually external to the cluster itself. In this case can use NodePort to expose ports for services and use the reverse proxy to manage the routing for the various services.  


````
kubectl get pods -l app=hello-world-mt -o wide
kubectl get pods -l app=hello-world-mt -o yaml | grep podIP
````

kubectl get svc hello-world-mt:
````
ubuntu@5gsol-master:~$ kubectl get svc hello-world-mt
NAME             TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
hello-world-mt   ClusterIP   10.110.37.212   <none>        80/TCP    3m8s
ubuntu@5gsol-master:~$
````


kubectl describe svc hello-world-mt:
````
ubuntu@5gsol-master:~$ kubectl describe svc hello-world-mt
Name:              hello-world-mt
Namespace:         default
Labels:            app=hello-world-mt
Annotations:       Selector:  app=hello-world-mt
Type:              ClusterIP
IP:                10.110.37.212
Port:              <unset>  80/TCP
TargetPort:        80/TCP
Endpoints:         10.36.0.1:80,10.36.0.2:80,10.44.0.1:80
Session Affinity:  None
Events:            <none>
ubuntu@5gsol-master:~$
````


kubectl get ep hello-world-mt:
````
ubuntu@5gsol-master:~$ kubectl get ep hello-world-mt
NAME             ENDPOINTS                                AGE
hello-world-mt   10.36.0.1:80,10.36.0.2:80,10.44.0.1:80   4m59s
ubuntu@5gsol-master:~$
````

````
ubuntu@5gsol-master:~$ kubectl get pods -l app=hello-world-mt -o wide
NAME                                        READY   STATUS    RESTARTS   AGE     IP          NODE                NOMINATED NODE   READINESS GATES
hello-world-mt-nginx-html-9b6767c7b-4tkmq   1/1     Running   0          8m27s   10.44.0.1   5gsol-slavenode-0   <none>           <none>
hello-world-mt-nginx-html-9b6767c7b-fk9cs   1/1     Running   0          8m32s   10.36.0.1   5gsol-slavenode-2   <none>           <none>
hello-world-mt-nginx-html-9b6767c7b-fkjcc   1/1     Running   0          8m30s   10.36.0.2   5gsol-slavenode-2   <none>           <none>
ubuntu@5gsol-master:~$
````
````
kubectl exec hello-world-mt-nginx-html-9b6767c7b-4tkmq -- printenv | grep SERVICE
````







## 7 - Optional - Install the Kubernetes Dashboard  
The dashboard by itself does not have enough permissions to make it usefull. The "recommended.yaml" file needs to be updated so that additional permissions can be added to the dashboard user role so that information of value can eb displayed on the dashboard. The 8001 port must be exposed from the master node. The proxy command below is run from a remote client where the "kubectl" config is present (and of course kubectl is installed) and commands to manage the cluster can be executed.  

There is some information flying about the internet about exporting the dashboard on an external IP adress but we dont need this. Original site: https://github.com/kubernetes/dashboard   
Info for permissions hack included below: https://github.com/kubernetes/dashboard/issues/4179  

Run on the master node: 
````
wget https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-rc7/aio/deploy/recommended.yaml
````

Modify the "recommended.yaml" file to include the following: 
````
# This is the existing part of the config...
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
rules:
  # Allow Metrics Scraper to get metrics from the Metrics server
  - apiGroups: ["metrics.k8s.io"]
    resources: ["pods", "nodes"]
    verbs: ["get", "list", "watch"]

# This is the part of the config to be added manually...
  # Other resources
  - apiGroups: [""]
    resources: ["nodes", "namespaces", "pods", "serviceaccounts", "services", "configmaps", "endpoints", "persistentvolumeclaims", "replicationcontrollers", "replicationcontrollers/scale", "persistentvolumeclaims", "persistentvolumes", "bindings", "events", "limitranges", "namespaces/status", "pods/log", "pods/status", "replicationcontrollers/status", "resourcequotas", "resourcequotas/status"]
    verbs: ["get", "list", "watch"]
  
  - apiGroups: ["apps"]
    resources: ["daemonsets", "deployments", "deployments/scale", "replicasets", "replicasets/scale", "statefulsets"]
    verbs: ["get", "list", "watch"]

  - apiGroups: ["autoscaling"]
    resources: ["horizontalpodautoscalers"]
    verbs: ["get", "list", "watch"]

  - apiGroups: ["batch"]
    resources: ["cronjobs", "jobs"]
    verbs: ["get", "list", "watch"]

  - apiGroups: ["extensions"]
    resources: ["daemonsets", "deployments", "deployments/scale", "networkpolicies", "replicasets", "replicasets/scale", "replicationcontrollers/scale"]
    verbs: ["get", "list", "watch"]

  - apiGroups: ["networking.k8s.io"]
    resources: ["ingresses", "networkpolicies"]
    verbs: ["get", "list", "watch"]

  - apiGroups: ["policy"]
    resources: ["poddisruptionbudgets"]
    verbs: ["get", "list", "watch"]

  - apiGroups: ["storage.k8s.io"]
    resources: ["storageclasses", "volumeattachments"]
    verbs: ["get", "list", "watch"]

  - apiGroups: ["rbac.authorization.k8s.io"]
    resources: ["clusterrolebindings", "clusterroles", "roles", "rolebindings", ]
    verbs: ["get", "list", "watch"]
````

From the master again: 
````
kubectl apply -f 
kubectl -n kubernetes-dashboard get secret
kubectl -n kubernetes-dashboard describe secrets kubernetes-dashboard-token-lp2bl
````

On the remote kubectl client execute the proxy for the dashboard:
````
nohup kubectl proxy --address="0.0.0.0" -p 8001 --accept-hosts='^*$' &
````

Use the token from the final command to log into the dashboard which can be found here: 
````
http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/ 
````

To kill the proxy:
````
ps -a
kill -9 <pid for kubectl>
````  

### 7.1 - Enabling the Skip Function
Download the **"recommended.yaml"** file from the source location: https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-rc7/aio/deploy/recommended.yaml  
Edit the file and add the following lines: 
````
- --enable-skip-login
- --disable-settings-authorizer
````
So that it looks like this:  
![KubernetesDashboard-EnableSkip01][KubernetesDashboard-EnableSkip01]  

Then import the modified/updated yaml file using the following:
````
kubectl apply -f recommended.yaml
````


If you make changes to the yaml file or to remove a previously imported yaml file:
````
kubectl delete clusterrolebinding kubernetes-dashboard
kubectl delete clusterrole kubernetes-dashboard
kubectl delete namespace kubernetes-dashboard
````  
Then the newer file can be imported.  

---
## 8 - Creating Users with Limited Permissions

Install the JSON Processor tool:  
https://stedolan.github.io/jq/download/  
````
sudo apt-get install jq
````

From this site:  
https://gardener.cloud/documentation/guides/client_tools/working-with-kubeconfig/ 

This script:  
````
#!/bin/bash

if [[ -z "$1" ]] ;then
  echo "usage: $0 <username>"
  exit 1
fi

user=$1
kubectl create sa ${user}
secret=$(kubectl get sa ${user} -o json | jq -r .secrets[].name)
kubectl get secret ${secret} -o json | jq -r '.data["ca.crt"]' | base64 -d > ca.crt

user_token=$(kubectl get secret ${secret} -o json | jq -r '.data["token"]' | base64 -d)
c=`kubectl config current-context`
cluster_name=`kubectl config get-contexts $c | awk '{print $3}' | tail -n 1`
endpoint=`kubectl config view -o jsonpath="{.clusters[?(@.name == \"${cluster_name}\")].cluster.server}"`

# Set up the config
KUBECONFIG=k8s-${user}-conf kubectl config set-cluster ${cluster_name} \
    --embed-certs=true \
    --server=${endpoint} \
    --certificate-authority=./ca.crt

KUBECONFIG=k8s-${user}-conf kubectl config set-credentials ${user}-${cluster_name#cluster-} --token=${user_token}
KUBECONFIG=k8s-${user}-conf kubectl config set-context ${user}-${cluster_name#cluster-} \
    --cluster=${cluster_name} \
    --user=${user}-${cluster_name#cluster-}
KUBECONFIG=k8s-${user}-conf kubectl config use-context ${user}-${cluster_name#cluster-}

cat <<EOF | kubectl create -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: view-${user}-global
  namespace: default
subjects:
- kind: ServiceAccount
  name: ${user}
  namespace: default
roleRef:
  kind: ClusterRole
  name: view
  apiGroup: rbac.authorization.k8s.io

EOF


echo "done! Test with: "
echo "export KUBECONFIG=k8s-${user}-conf"
echo "kubectl get pods"
````

Set script as executable: 
````
chmod +x createUser.sh
```` 

````
kubectl delete serviceaccount/appart
kubectl delete clusterrolebinding.rbac.authorization.k8s.io/view-appart-global

kubectl delete serviceaccount/appart
kubectl delete rolebinding.rbac.authorization.k8s.io/view-appart-global
````



---
---

## Appendix I - Complete Output from the kubeadm init and join commands
````
root@5gsol-master:/home/ubuntu/5GSolutions# kubeadm init
W0617 13:00:18.206898    6917 configset.go:202] WARNING: kubeadm cannot validate component configs for API groups [kubelet.config.k8s.io kubeproxy.config.k8s.io]
[init] Using Kubernetes version: v1.18.3
[preflight] Running pre-flight checks
[preflight] Pulling images required for setting up a Kubernetes cluster
[preflight] This might take a minute or two, depending on the speed of your internet connection
[preflight] You can also perform this action in beforehand using 'kubeadm config images pull'
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Starting the kubelet
[certs] Using certificateDir folder "/etc/kubernetes/pki"
[certs] Generating "ca" certificate and key
[certs] Generating "apiserver" certificate and key
[certs] apiserver serving cert is signed for DNS names [5gsol-master kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local] and IPs [10.96.0.1 172.16.4.88]
[certs] Generating "apiserver-kubelet-client" certificate and key
[certs] Generating "front-proxy-ca" certificate and key
[certs] Generating "front-proxy-client" certificate and key
[certs] Generating "etcd/ca" certificate and key
[certs] Generating "etcd/server" certificate and key
[certs] etcd/server serving cert is signed for DNS names [5gsol-master localhost] and IPs [172.16.4.88 127.0.0.1 ::1]
[certs] Generating "etcd/peer" certificate and key
[certs] etcd/peer serving cert is signed for DNS names [5gsol-master localhost] and IPs [172.16.4.88 127.0.0.1 ::1]
[certs] Generating "etcd/healthcheck-client" certificate and key
[certs] Generating "apiserver-etcd-client" certificate and key
[certs] Generating "sa" key and public key
[kubeconfig] Using kubeconfig folder "/etc/kubernetes"
[kubeconfig] Writing "admin.conf" kubeconfig file
[kubeconfig] Writing "kubelet.conf" kubeconfig file
[kubeconfig] Writing "controller-manager.conf" kubeconfig file
[kubeconfig] Writing "scheduler.conf" kubeconfig file
[control-plane] Using manifest folder "/etc/kubernetes/manifests"
[control-plane] Creating static Pod manifest for "kube-apiserver"
[control-plane] Creating static Pod manifest for "kube-controller-manager"
W0617 13:00:42.290100    6917 manifests.go:225] the default kube-apiserver authorization-mode is "Node,RBAC"; using "Node,RBAC"
[control-plane] Creating static Pod manifest for "kube-scheduler"
W0617 13:00:42.291336    6917 manifests.go:225] the default kube-apiserver authorization-mode is "Node,RBAC"; using "Node,RBAC"
[etcd] Creating static Pod manifest for local etcd in "/etc/kubernetes/manifests"
[wait-control-plane] Waiting for the kubelet to boot up the control plane as static Pods from directory "/etc/kubernetes/manifests". This can take up to 4m0s
[apiclient] All control plane components are healthy after 20.001940 seconds
[upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config-1.18" in namespace kube-system with the configuration for the kubelets in the cluster
[upload-certs] Skipping phase. Please see --upload-certs
[mark-control-plane] Marking the node 5gsol-master as control-plane by adding the label "node-role.kubernetes.io/master=''"
[mark-control-plane] Marking the node 5gsol-master as control-plane by adding the taints [node-role.kubernetes.io/master:NoSchedule]
[bootstrap-token] Using token: wlfkgq.ciek5xntqmimls1w
[bootstrap-token] Configuring bootstrap tokens, cluster-info ConfigMap, RBAC Roles
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to get nodes
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstrap-token] configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstrap-token] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[bootstrap-token] Creating the "cluster-info" ConfigMap in the "kube-public" namespace
[kubelet-finalize] Updating "/etc/kubernetes/kubelet.conf" to point to a rotatable kubelet client certificate and key
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 172.16.4.88:6443 --token wlfkgq.ciek5xntqmimls1w \
    --discovery-token-ca-cert-hash sha256:ae3c511cfaedc95c0ef4bdaaff2eaeeff6e0e3249d5dba344efd95903278487f
root@5gsol-master:/home/ubuntu/5GSolutions#

======================
kubeadm join 172.16.4.88:6443 --token wlfkgq.ciek5xntqmimls1w --discovery-token-ca-cert-hash sha256:ae3c511cfaedc95c0ef4bdaaff2eaeeff6e0e3249d5dba344efd95903278487f
======================


=============== Slave 0 ========================================
root@5gsol-slavenode-0:/home/ubuntu/5GSolutions# kubeadm join 172.16.4.88:6443 --token wlfkgq.ciek5xntqmimls1w --discovery-token-ca-cert-hash sha256:ae3c511cfaedc95c0ef4bdaaff2eaeeff6e0e3249d5dba344efd95903278487f
W0617 13:07:25.411666   17589 join.go:346] [preflight] WARNING: JoinControlPane.controlPlane settings will be ignored when control-plane flag is not set.
[preflight] Running pre-flight checks
[preflight] Reading configuration from the cluster...
[preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -oyaml'
[kubelet-start] Downloading configuration for the kubelet from the "kubelet-config-1.18" ConfigMap in the kube-system namespace
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Starting the kubelet
[kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap...

This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the control-plane to see this node join the cluster.

root@5gsol-slavenode-0:/home/ubuntu/5GSolutions#



=============== Slave 1 ========================================
root@5gsol-slavenode-1:/home/ubuntu/5GSolutions# kubeadm join 172.16.4.88:6443 --token wlfkgq.ciek5xntqmimls1w --discovery-token-ca-cert-hash sha256:ae3c511cfaedc95c0ef4bdaaff2eaeeff6e0e3249d5dba344efd95903278487f
W0617 13:12:22.652444   17701 join.go:346] [preflight] WARNING: JoinControlPane.controlPlane settings will be ignored when control-plane flag is not set.
[preflight] Running pre-flight checks
[preflight] Reading configuration from the cluster...
[preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -oyaml'
[kubelet-start] Downloading configuration for the kubelet from the "kubelet-config-1.18" ConfigMap in the kube-system namespace
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Starting the kubelet
[kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap...

This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the control-plane to see this node join the cluster.

root@5gsol-slavenode-1:/home/ubuntu/5GSolutions#


=============== Slave 2 ========================================
root@5gsol-slavenode-2:/home/ubuntu/5GSolutions# kubeadm join 172.16.4.88:6443 --token wlfkgq.ciek5xntqmimls1w --discovery-token-ca-cert-hash sha256:ae3c511cfaedc95c0ef4bdaaff2eaeeff6e0e3249d5dba344efd95903278487f
W0617 13:16:27.450404   17659 join.go:346] [preflight] WARNING: JoinControlPane.controlPlane settings will be ignored when control-plane flag is not set.
[preflight] Running pre-flight checks
[preflight] Reading configuration from the cluster...
[preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -oyaml'
[kubelet-start] Downloading configuration for the kubelet from the "kubelet-config-1.18" ConfigMap in the kube-system namespace
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Starting the kubelet
[kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap...

This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the control-plane to see this node join the cluster.

root@5gsol-slavenode-2:/home/ubuntu/5GSolutions#
````


[MasterNode-5GSolDirectory01]: Assets/MasterNode-5GSolDirectory01.jpg?raw=true "MasterNode-5GSolDirectory01"
[NewlyCreatedK8nCluster_BasicCommandsExecuted]: Assets/NewlyCreatedK8nCluster_BasicCommandsExecuted.jpg?raw=true "NewlyCreatedK8nCluster_BasicCommandsExecuted"
[KubernetesDashboard-EnableSkip01]: Assets/KubernetesDashboard-EnableSkip01.jpg?raw=true "KubernetesDashboard-EnableSkip01"

---  
---  